module Parser where

import Prelude
import Grammar
import Program
import Lexer

fullParse :: String -> Program	
fullParse s = fst $ parseProgram $ fullLex s

parseProgram :: LexedList -> (Program, LexedList)
parseProgram [] 	= error "PARSE STUPIDITY: please enter some code to parse" 
parseProgram a		= (P c, r)
					where (c,r) = parseCodeBlock As a
					
parseCodeBlock :: State -> LexedList -> (CodeBlock, LexedList)  -- the program is a codeblock, but {} around the full program is unnecessary,
parseCodeBlock _ [] = (Cb [], [])								
parseCodeBlock As a = (Cb c, r)									-- State As indicates that the code block should not have curly braces
					where 							
						(c,r) 	= parseStatements a
parseCodeBlock Bs a = (Cb c, rD)								-- State Bs indicates that the code block should have curly braces
					where 	
						(nC,rC) = parseType LC a 
						(c,r) 	= parseStatements rC
						(nD,rD) = parseType RC r
															
parseStatements :: LexedList -> ([Statement], LexedList)
parseStatements [] 	= ([],[])
parseStatements a 	| (fst (a!!0) == LC) && (fst (a!!1) == RC) 	= ([],drop 2 a)	--check if the code block is empty: [(LC,""),(RC,"")]
					| rs == [] || fst(rs!!0) == RC				= ([s],rs)		--check whether the remaining statements are empty [(RC,"")]
					| otherwise 								= ((s:ss),rss)	--parse the next statement
					where 
						(s,rs) = parseStatement a
						(ss,rss) = parseStatements rs
											
parseStatement :: LexedList -> (Statement, LexedList)
parseStatement ((a,b):xs)	|	a == ID 	= (Ass nAA1, rAT2)
							|	a == IF		= (If nIE1 nIC2, rIC2)
							|	a == FOR	= (For nFA2 nFE4 nFA6 nFC8, rFC8)
							|	a == WHILE	= (While nWE2 nWC4, rWC4)
							|	otherwise 	= error("PARSE ERROR: Invalid statement, expected Identifier, If, For or While, got " ++ show a)
							where 
																		--Assignment
								(nAA1,rAA1)	= parseAssignment ((a,b):xs)-- a = 5
								(nAT2,rAT2)	= parseType SC rAA1			-- ;
								
																		--if
								(nIE1,rIE1)	= parseExpression xs		-- (a>5)
								(nIC2,rIC2)	= parseCodeBlock Bs rIE1	-- { }
								
																		--for		
								(nFT1,rFT1)	= parseType LB xs			-- (
								(nFA2,rFA2)	= parseAssignment rFT1		-- a=5
								(nFT3,rFT3)	= parseType SC rFA2			-- ;
								(nFE4,rFE4) = parseExpression rFT3		-- (a<3)
								(nFT5,rFT5)	= parseType SC rFE4			-- ;
								(nFA6,rFA6)	= parseAssignment rFT5		-- (a=5)		
								(nFT7,rFT7)	= parseType RB rFA6			-- )
								(nFC8,rFC8)	= parseCodeBlock Bs rFT7	-- { }
								
																		--while
								(nWT1,rWT1)	= parseType LB xs			-- (
								(nWE2,rWE2)	= parseExpression rWT1		-- (a > 4)
								(nWT3,rWT3)	= parseType RB rWE2			-- (
								(nWC4,rWC4)	= parseCodeBlock Bs rWT3	-- { }


parseAssignment :: LexedList -> (Assignment, LexedList)							
parseAssignment a = ((A nI nE),rE)
					where 
						(nI,rI) = parseId a
						(nC,rC) = parseType Eq rI
						(nE,rE) = parseExpression rC

parseId :: LexedList -> (Id, LexedList)
parseId ((a,b):xs) 	| a == ID 		= ((Id b),xs)
					| a == Integer 	= ((Num (read b)),xs)
					| otherwise		= error ("PARSE ERROR: expected ID or integer, got: " ++ show a)



parseExpression :: [(LexType,String)] -> (Expression, [(LexType,String)])
parseExpression ((a,b):xs) 	| (a == ID || a == Integer) =((S nS),rS)
							| (a == LB)					=((C nE3 nO3 nE4),rT2)
							| otherwise 				= error ("PARSE ERROR: expected ID, integer or (, got: " ++ show a)
									where
										(nS,rS) 	= parseId ((a,b):xs)
										
										(nT1,rT1)  	= parseType LB ((a,b):xs)
										(nE3,rE3) 	= parseExpression rT1
										(nO3,rO3)	= parseType OP rE3
										(nE4,rE4)	= parseExpression rO3
										(nT2,rT2)   = parseType RB rE4
							
								
parseType :: LexType -> LexedList -> (String, LexedList)
parseType a []										= error ("PARSE ERROR: End of stream, expecting "++(show a))
parseType a ((b,c):xs) 	|	(b == a) && (b == OP) 	= (c,xs) 
						|	(b == a) && (b /= OP)	= ((show b),xs) 
						|	otherwise 				= error ("PARSE ERROR:  Wrong character: got "++(show b)++", expecting "++(show a))
