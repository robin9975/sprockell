
import Grammar
import Program
import Parser
import Compiler
import System.IO
import Prelude
import Exec

-- Compiles a file to a set of SPROCKELL Instructions, based on a file
compileFile s = do
	h <- openFile s ReadMode
	x <- hGetContents h
	return (compileP $ fullParse x) 

-- Runs the compiled file using the SPROCKELL simulator
run file = do
	x <- compileFile file
	sim testAddr x  