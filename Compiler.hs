{--Compiler module--}
module Compiler where

import Prelude
import Grammar
import TypesEtc
import Exec 

type VarRegister = [([Char], Int)]			-- Lookup table for variable registers
type Addr 		 = Int						

{- Computer an expression in the register. Final value ends up in the register address specified. 
	Variables gets loaded from the memory, NOT from the register. -}
compileE :: Expression -> VarRegister -> Addr -> ([Assembly], Addr)
compileE (S (Num x)) vr	addr		= ([Load (Imm x) addr], addr)
compileE (S (Grammar.Id x))	vr addr = ([Load (Addr (getReg vr x)) addr], addr)
compileE (C e1 op e2) vr addr		= (v1 ++ v2 ++ [Compute (opToF op) a1 a2 addr], addr)
										where
											(v1, a1)	= compileE e1 vr addr
											(v2, a2)	= compileE e2 vr (addr+1)

{- Compiles the program datatree into a list of SPROCKELL instructions -}
compileP :: Program -> [Assembly]
compileP (P cb)	= fst (compileBlock cb []) ++ [EndProg] -- Statements

{- Compiles a codeblock into a list of SPROCKELL instructions, taking into account the VarRegister -}
compileBlock :: CodeBlock -> VarRegister -> ([Assembly], VarRegister)
compileBlock (Cb []) vr		= ([], vr)
compileBlock (Cb (s:ss)) vr	= (statements ++ newStatements, vr'') 
								where
									(statements, vr') 		= compileStatement s vr
									(newStatements, vr'') 	= compileBlock (Cb ss) vr'

{- Compiles a statement. After an assignment, a updated VarRegister is send back, in further use for compiling. -}
compileStatement :: Statement -> VarRegister -> ([Assembly], VarRegister)
compileStatement (Ass (A (Grammar.Id id) exp)) vr 	= (fst (compileE exp vr 1) ++ [Store (Addr 1) newaddr], vr ++ [(id, newaddr)])
													where
														newaddr = getReg vr id

compileStatement (If exp cb) vr						= (fst (compileE invExp vr 1) ++ [Jump CR nlines] ++ block, vr')
													where
														(block, vr') 	= compileBlock cb vr
														nlines 			= (length block) + 1
														invExp			= (C e1 (invOp op) e2)
														(C e1 op e2)	= exp

compileStatement (IfElse exp cb1 cb2) vr 			= (exp1 ++ [Jump CR nlines1] ++ block1 ++ exp2 ++ [Jump CR nlines2] ++ block2, vr'')
													where
														(exp1, eaddr) 	= compileE invExp vr 1
														(exp2, eaddr2)	= compileE (C e1 op e2) vr 1
														(block1, vr')	= compileBlock cb1 vr
														(block2, vr'')	= compileBlock cb2 vr'
														nlines1 		= length block1 + length exp2 + 2
														nlines2			= length block2 + 1
														invExp			= (C e1 (invOp op) e2)
														(C e1 op e2)	= exp

compileStatement (For ass1 exp ass2 cb) vr			= (a1 ++ e1 ++ [Jump CR nlinesfwd] ++ a2 ++ block ++ [Jump UR (-nlinesback)], vr''')
													where
														invExp			= (C el (invOp op) er)
														(C el op er)	= exp
														(a1, vr')		= compileStatement (Ass ass1) vr
														(e1, addr)		= compileE invExp vr' 1
														(a2, vr'')		= compileStatement (Ass ass2) vr'
														(block, vr''') 	= compileBlock cb vr''
														nlinesfwd		= length block + length a2 + 2
														nlinesback		= length block + length e1 + length a2 + 1
														

compileStatement (While exp cb) vr					= (e1 ++ [Jump CR nlinesfwd] ++ block ++ [Jump UR (-nlinesback)], vr')
													where
														invExp			= (C el (invOp op) er)
														(C el op er)	= exp
														(e1, addr)		= compileE invExp vr 1
														(block, vr')	= compileBlock cb vr 
														nlinesfwd		= length block + 2
														nlinesback		= length block + length e1 + 1



{-- Converse an operator into a SPROCKELL function -}
opToF :: [Char] -> OpCode
opToF x 	= case x of 	
				"+" 	-> Add
				"-"		-> Sub
				"*"		-> Mul
				"/"		-> Div
				"mod"	-> Mod 
				"==" 	-> Equal
				"!="	-> NEq
				">"		-> Gt
				"<"		-> Lt
				"&&"	-> And
				"||"	-> Or

{-- inverses the operator, for use in if statements and contidional jumps --}
invOp :: [Char] -> [Char]
invOp x		= case x of 
				"==" -> "!="
				"!=" -> "=="
				">"	-> "<"
				"<"	-> ">"

{- Gets the memory address for a variable. When it is not present, it returns a new memory value. 
The minumum value it returns is 7, where the registes starts  -}
getReg :: VarRegister -> [Char] -> Int
getReg vr id 	= hgetReg vr 6 id

-- Returns the register corresponding to the variable, or the highest value of register that is used + 1 (aka a new register for a new variable)
hgetReg :: VarRegister -> Int -> [Char] -> Int
hgetReg [] maxReg _ 				= maxReg + 1
hgetReg ((id, addr):vr) maxReg x 	| x == id 		= addr
									| otherwise 	= hgetReg vr newMax x
									where
										newMax = if addr > maxReg then addr else maxReg