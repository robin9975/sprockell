# SPROCKELL
Martijn Bakker & Robin Hoogervorst -- October 2014

## How to run (main.hs, Program.hs)

The Program module has some test programs defined as variables. We have three different tests
* Strings to test the parser (testProgram, testProgram2)
* Trees, to test the compiler (progTree1, progTree2, progTree3)
* Files, for the full fetched test (1.sp, 2.sp, 3.sp /testscripts)

To test a string to tree conversion:

	fullParse s

Where s is a string with instructions. 

To get a list of assembly instructions from a test tree
	
	compileP <tree>
	compileP progTree1

To get a set of assembly instructions from the code, we just combine these two functions

	compileP $ fullParse s

The last functionality is to read a file, and compile this or run this directly using the SPROCKELL simulator. This can be done using

	compileFile <file>
	compileFile "testscripts/1.sp"

	run <file>
	run "testscripts/1.sp"


We defined testAddr with a large range of memory to view, so we have enough space to view our variables. This is also used in the run of main.hs. Of course, another testAddress can be specified as shown in Exec.hs.

Testscript 1.sp is the same as the first example in Exec.hs (compute 3^5). When we run this using the function above, we get indeed 243.

## Expression bracket problem
Unfortunately, our expression syntax is not totally the way we wanted it to be. Using our parses, we only could get it to work if combined expressions use brackets around it. So

	a = 4+3;

would not work, since 4+3 is a combined expression. Instead, we write:

	a = (4+3);

For single expressions (only an Id or Number), this problem does not exist. Please keep in mind that this is also the case for while statements (which expect an expression between its own brackets, and thus double brackets). It is kind of ugly, but after trying and trying, we could not get it to work without these brackets. Few examples of right statements:

	a = 10;
	while ((a<10)) {a = a-1;}
	a = (3+10);
	a = ((1*19)*(4+(2+3)));

## Lexer (Lexer.hs)
The preprocessor for our parser, the lexer strips down the input program string according to checking whether the beginning of the string matches one of the possible starts of an element, for instance an 'a' would indicate the start of a variable identifier whereas '1' indicates the start of a number, which are then further stripped from the input by finite state machines. The operators are simply pattern matched against a list of known operators and all of the other important characters, brackets, '=' are also stripped off by simple checks. Afterwards, keywords like 'while' are still recognized as a variable name, so we run another function which replaces all variable identifier by their keyword element counterparts. The final output of our lexer is of the type LexedList, which is defined to be a [(Lextype,String)]. In this way we can, for instance, store (Identifier,"a"), (FOR,"") or (Integer,"93"). Specific notes on decisions can be found in the source code.

## Parser (Parser.hs)
The parser is a very straightforward parser which recursively parses the generated tokens from the lexer in order to generate a program tree according to our specified grammar in Grammar.hs. Specific notes on decisions can be found in the source code.

## Rules for writing a test program
Our language is a very simple imperative language. It consists of a list of statements:
1) Assignments

	a = 10;
	a = ((10+10) > (20*10));
	
It should be noted that the form is always <variable> '=' <expression>, in which a compound expression, ie. a+b should always be enclosed by brackets : (a+b). Furthermore, every separate assignment as statement should be terminated by a semicolon.

2) For loops		

	for(a=0;(a<10);a=(a+1)){}

For loops consist of the following syntax: 'for' '(' <assignment> ';' <expression> ';' <assignment> ')' '{' [<statement>] '}'.

3) While loops

	while((a<100)){}
	
While loops are again very similar to C: 'while' '(' <expression> ')' '{' [<statement>] '}'

4) If statements

	if((a<100)){}
	
If statements are very basic: 'if' '(' <expression> ')' '{' [<statement>] '}'


## Datatree (Grammar.hs)
Our data tree is defined in the module Grammar (Grammar.hs). The main structure is defined as a tree, and we define an operator to be a list of characters, so we can also have operators like '==' and '!='. Furthermore, the types are pretty straightforward.

## Compiler (Compiler.hs)
There are two main functions to compile. CompileE compiles an expression and CompileP compiles an total program, based on the datatree. 

The compiler uses the memory for storing variables and the register for performing computations. When an expression gets compiled, the compiler specifies the address it wants the result to go to. Then it can decide to Store it as a variable to the memory or use the first register to use the result for an conditional jump. 

The expression compiler does not use the POP and PUSH instruction. We did not get it to work totally with these instructions, due to unclarity of examples and documentation and found out only last friday at 15:00 that the PUSH and POP uses a register address as place to compile. Instead, we used an custom alternative that uses the registry as some kind of stack. In an compound expression, it stores the values starting at registry 1, the second value in registry 2, computes these and goes on computing the rest. Example:

	(3+4) + (5+6)
	//store 3 in reg 1
	//store 4 in reg 2
	//compute reg1 + reg2 to reg 1 (3+4)
	//store 5 in reg 2
	//store 6 in reg 3
	//compute reg2 + reg3 to reg2 (5+6)
	//compute reg1 + reg2 to reg1 (7+11)

We are aware that this could lead complications with large expressions, but a simle way to solve this in this case is to increase the register size in the SPROCKELL simulator. This is not very nice, but in the end, we did not have the time to rewrite the function to PUSH and POP instructions.

During compiling, the compiler keeps a Variable Register, that is defined as its own type. This gets updated as variables get assigned and gets send to the compile expression function, so the expression compiler knows the location of variables in the memory. 

### Pattern mathing
We decided to use pattern matching instead of case situations, because we have a lot of where clauses that were different for each specific case. There are some similarities, but whe found out that this was the most neat way to keep it a bit structured to ourselves.

### Jumps
In the compiler, relative jumps are used for the statements. Using the length function in haskell, we know the amount of Assembly functions that we have, so we know where to jump to. Sometimes, we need a +1, or +2 to correct for the jump statements itelf (a for loop would then jump to its own jump back statement, which would lead to an infinite loop).

### Statements
During statements, we used relative conditional jumps. By knowing the amount of lines Assembly code we get returned, we know the amount of lines we need to jump. We inverse the operator using a function, for use of the conditional jumps, since the code needs to jump to another place if the statement is false. Furthermore, in an expression the register could change, so we keep sending the Variable Register through during compiling. 

### VarRegister
We defined a variable register, which stores the memory address of the variables in the register. These can be looked up by the compiler. Its type is of tuples from strings to integers (aka [([Char], Int)]).
