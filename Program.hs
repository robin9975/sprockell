module Program where

import Grammar

testProgram = "{\
\a = 10;\
\i = 2;\
\c = 3;\
\while (a > 0) {\
\i = (i*1);\
\a = (a-1);\
\}\
\if(c > 1) {\
\b = 0;\
\d = 0;\
\}\
\testvariabele = 100;\
\b = 6;\
\c = (5*3);\
\i = 1;\
\j = 1;\
\d = 10;\
\for(d = 0; (d < 10); d=(d+1)) {\
\d = (d*3);\
\}"

testProgram2 = "c=5;c=(5+6);"
-- =======

testAddr	 = ([1..6], [7..12]);

-- Test trees

-- For loop test
progTree1 	= (P (Cb [
					(For 
						(A (Id "a") (S (Num 4)))
						(C (S (Id "a")) "==" (S (Num 4)))
						(A (Id "a") (C (S (Id "a")) "+" (S (Num 1))))
						(Cb [(Ass (A (Id "b") (C (S (Num 4)) "+" (S (Num 3)))))])
					), 
					(Ass (A (Id "a") (C (S (Num 4)) "+" (S (Num 3)))))
					]))

-- While loop test, same program as 1 from Exec
progTree2 	= (P (Cb [
					(Ass (A (Id "a") (S (Num 3)))),
					(Ass (A (Id "n") (S (Num 5)))),
					(Ass (A (Id "power") (S (Num 1)))),
					(While 
						(C (S (Id "n")) "!=" (S (Num 0)))
						(Cb [
							(Ass (A (Id "power") 	(C (S (Id "a")) "*" (S (Id "power"))))),
							(Ass (A (Id "n") 		(C (S (Id "n")) "-" (S (Num 1)))))
							])
					)
					]))
-- IfElse test
progTree3 	= (P (Cb [
					(Ass (A (Id "a") (S (Num 10)))),
					(IfElse 
						(C (S (Id "a")) "==" (S (Num 10)))
						(Cb [
							(Ass (A (Id "b") (S (Num 100))))
							])
						(Cb [
							(Ass (A (Id "b") (S (Num 50))))
							])
					) 
					]))

-- A nested expression test
exprTree1 	= (C (C (S (Id "a")) "/" (S (Num 80))) "-" (C (S (Num 4)) "+" (C (S (Num 10)) "*" (S (Num 22)))))
