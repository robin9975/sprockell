{-- Grammar Definitions for our compiler --}
module Grammar where

import Prelude

data Program		= P CodeBlock 				deriving(Show)
data CodeBlock		= Cb [Statement] 			deriving(Show)
data Statement 		= Ass Assignment 
					| If Expression CodeBlock 
					| IfElse Expression CodeBlock CodeBlock
					| For Assignment Expression Assignment CodeBlock
					| While Expression CodeBlock deriving(Show)

data Expression 	= S Id											-- Single
					| C Expression Op Expression deriving(Show)		-- Combined

-- Id is an identifier which can be just a number or a variable, for use as terminal in expressions
data Id 			= Id [Char] | Num Int 		deriving(Show)
data Assignment 	= A Id Expression 			deriving(Show)

type Op = [Char]



data LexType = Integer | ID | Eq | OP | SC | LB | RB | LC | RC | IF | WHILE | FOR | ELSE deriving(Show,Eq)
data State = As | Bs