module Lexer where

import Prelude
import Grammar
import Data.List

type LexedList = [(LexType,String)]

--definitions of characters that may appear in the program
letters = ['a'..'z']++['A'..'Z']
numbers = ['0'..'9']
eq = ['=']
sc = [';']
op = ["*","+","-","+","/",">",">=","==","<=","<","!=","||","&&"]
lb = ['(']
rb = [')']
lc = ['{']
rc = ['}']
space = [' ','\n','\t','\r']

--main lexer function
fullLex :: String -> LexedList	
fullLex s = fixkey . fst $ lexf s

--pass over the LexedList again in order to replace keyword variables with keyword nodes
fixkey :: LexedList -> LexedList
fixkey [] = []
fixkey ((a,b):xs) 	| b == "if"		= (IF, []):fixkey xs	
					| b == "else"	= (ELSE, []):fixkey xs
					| b == "for"	= (FOR, []):fixkey xs
					| b == "while"	= (WHILE, []):fixkey xs
					| otherwise		= (a,b):fixkey xs
					
--auxilary function, checking whether a string starts with a list of possibilities, returning true/false, the match and the remaining values
--this is used in order to strip the operators from the program string, who may contain multiple characters per operator.
lexStart :: String -> [String] -> (Bool, String, String)
lexStart str []							= (False, [], [])
lexStart str (s:ss)	| isPrefixOf s str 	= (True, s, drop (length s) str)
					| otherwise			= lexStart str ss

--the main lexing function
lexf :: String -> (LexedList,String)
lexf [] 		= ([],[])
lexf (x:xs)		|	elem x numbers 				= ([(Integer, n1)]++n3	,r3)
				|	elem x (numbers++letters) 	= ([(ID, n2)]++n4		,r4)
				|	b6							= ([(OP, n6)]++n7		,r7)
				|	elem x eq					= ([(Eq, [])]++n5		,r5)
				|	elem x sc					= ([(SC, [])]++n5		,r5)
				|	elem x lb					= ([(LB, [])]++n5		,r5)
				|	elem x rb					= ([(RB, [])]++n5		,r5)
				|	elem x lc					= ([(LC, [])]++n5		,r5)
				|	elem x rc					= ([(RC, [])]++n5		,r5)
				|	elem x space				= lexf xs
				| 	otherwise					= ([]					,(x:xs))
				where 
					-- lex a number
					(n1,r1) = lexnumber (x:xs)
					(n3,r3) = lexf r1
					
					-- lex an identifier
					(n2,r2) = lexid As (x:xs)
					(n4,r4) = lexf r2
					
					-- lex an operator
					(b6,n6,r6) = lexStart (x:xs) op
					(n7,r7) = lexf r6
					
					-- lex a single character (Eq,SC,LB,RB,LC,RC)
					(n5,r5) = lexf xs
					
					
-- finite state machine for lexing numbers				
lexnumber :: String -> (String, String)
lexnumber []						= ([],[])
lexnumber (x:xs)|	elem x numbers 	= (x:n, r) 
				|	otherwise		= ([], (x:xs))
				where (n,r) = lexnumber xs

-- finite state machine for lexing identifiers
lexid :: State -> String -> (String, String)
lexid _ []							= ([],[])
lexid As (x:xs)	|	elem x letters 	= (x:n, r) 
				|	otherwise		= ([], (x:xs))
				where (n,r) = lexid Bs xs
lexid Bs (x:xs)	|	elem x (numbers++letters)	= (x:n, r) 
				|	otherwise					= ([], (x:xs))
				where (n,r) = lexid Bs xs
